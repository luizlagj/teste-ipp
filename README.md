Entrar no portal https://anypoint.mulesoft.com/login/#/signin clicar em API Manager e criar uma API de nome cadastro-<SEU_USER>. 
Utilizar o RAML para documentar o recurso cliente, conforme abaixo 

h4. Recurso -> cliente 
||Metodo||Path||Http status|| 
|GET| /{id }| 200, 404, 500| 
|POST| N/A | 201, 409, 500| 

Para verificar o significado de cada status http usar [este link|https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html] 

h4. Respostas 

Lembro que estes abaixo são exemplos, e para cada um deles deve conter o respectivo [schema json|http://json-schema.org/] com descrições do que são cada campo, no caso de erro alem de retornar o Http status serão criados códigos específicos para cada um deles e deverão ser documentados 

Sucesso 

{code:javascript} 
{ 
  "cpf": "12312312345", 
  "nome": "Joao das Neves", 
  "dataDeNascimento": "1986-12-26", 
  "endereco": { 
    "rua": "Rua do João", 
    "numero": 123, 
    "complemento": "Casa" 
  } 
} 
{code} 

Erro 

{code:javascript} 
{ 
  "mensagem": "Erro ao cadastrar", 
  "codigo": 123 
} 
{code} 

Para o cadastro o request é o abaixo, onde a única estrutura não obrigatória é a de endereço
{code:javascript} 
{ 
  "cpf": "12312312345", 
  "nome": "Joao das Neves", 
  "dataDeNascimento": "1986-12-26", 
  "endereco": { 
    "rua": "Rua do João", 
    "numero": 123, 
    "complemento": "Casa" 
  } 
} 
{code} 

Na resposta caso sucesso incluir o cabeçalho Location com o link para acessar o novo recurso criado